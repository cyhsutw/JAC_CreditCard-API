require 'json'
require 'dalli'
require 'sinatra'
require 'config_env'
require_relative 'model/credit_card.rb'
require_relative 'helpers/credit_card_api_helper.rb'

# credit card api service
class CreditCardAPI < Sinatra::Base
  include CreditCardApiHelper

  enable :logging

  configure :development, :test do
    ConfigEnv.path_to_config("#{__dir__}/config/config_env.rb")
    file = File.new("#{settings.root}/log/#{settings.environment}.log", 'a+')
    file.sync = true
    use Rack::CommonLogger, file
  end

  configure do
    set :cc_cache, Dalli::Client.new((ENV['MEMCACHIER_SERVERS'] || '').split(','),
                                     username: ENV['MEMCACHIER_USERNAME'],
                                     password: ENV['MEMCACHIER_PASSWORD'],
                                     socket_timeout: 1.5,
                                     socket_failure_delay: 0.2)
  end

  get '/' do
    {
      status: 'Up and running.'
    }.to_json
  end

  get '/api/v1/credit_card/validate/?' do
    number = params[:card_number]
    halt 400 unless number
    card = CreditCard.new
    card.number = number
    {
      card: number,
      validated: card.validate_checksum
    }.to_json
  end

  get '/api/v1/credit_card/?' do
    halt 401 unless authenticate && owner?(params[:user_id])
    content_type :json
    cache_cards.to_json
  end

  post '/api/v1/credit_card/?' do
    content_type :json
    halt 401 unless authenticate && owner?(params[:user_id])

    request_json = request.body.read
    unless request_json.empty?
      begin
        obj = JSON.parse(request_json)
        card = CreditCard.new(
          expiration_date: obj['expiration_date'],
          owner: obj['owner'],
          credit_network: obj['credit_network']
        )
        card.user_id = @user_id.to_i
        card.number = obj['number'].chomp
        if card.validate_checksum && card.save
          cache_cards(card)
          status 201
          body({
            status: 201,
            message: 'Created'
          }.to_json)
        else
          status 410
          body({
            status: 410,
            message: 'Gone'
          }.to_json)
        end
      rescue
        halt 400, {
          status: 400,
          message: 'Bad Request'
        }.to_json
      end
    end
  end

  get '/api/v1/credit_card/all/?' do
    begin
      CreditCard.all.map(&:to_hash).to_json
    rescue
      halt 500
    end
  end
end
