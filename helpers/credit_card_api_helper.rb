# helper
require 'jwt'
require 'base64'

# helper
module CreditCardApiHelper
  def authenticate
    begin
      scheme, jwt = env['HTTP_AUTHORIZATION'].split(' ')
      web_pub_key = OpenSSL::PKey::RSA.new(Base64.urlsafe_decode64(ENV['WEB_RSA_PUB_KEY']))
      payload, _header = JWT.decode(jwt, web_pub_key)
      @user_id = payload['sub'].to_i
      [
        scheme =~ /^Bearer$/i,
        payload['iss'] == ENV['WEB_APP_BASE_URL']
      ].reduce { |a, e| a && e }
    rescue
      false
    end
  end

  def owner?(user_id)
    user_id.present? && user_id.to_i == @user_id.to_i
  end

  def cc_cache
    settings.cc_cache
  end

  def cache_cards(cc = nil)
    cards = cc_cache.get(@user_id.to_s)
    if cards.nil?
      cards = CreditCard.of_user(@user_id).map(&:to_hash)
      cc_cache.set @user_id.to_s, cards.to_json
    elsif cc.present?
      cards = JSON.parse(cards)
      cards << cc.to_hash
      cc_cache.set @user_id.to_s, cards.to_json
    else
      cards = JSON.parse(cards)
    end
    cards
  end
end
